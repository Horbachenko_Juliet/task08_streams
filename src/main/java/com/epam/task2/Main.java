package com.epam.task2;

public class Main {

    public void print(Command c, String s){
        c.execute(s);
    }

    public static void main(String[] args) {
         Command command = s -> System.out.println(s);
        command.execute("Lambda");

        Main main1 = new Main();
        main1.print(AnnonymousClass::print, "Method reference");

        Command command1 = new Command() {
            @Override
            public void execute(String s) {
                System.out.println("Anonymous class");
            }
        };
       command1.execute("some string");
       CommandImpl command2 = new CommandImpl();
       command2.execute("Command object");
    }
}



