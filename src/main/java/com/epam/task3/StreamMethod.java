package com.epam.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamMethod {
    private List<Integer> integerList;
    private List<Integer> statistics;
    private Stream<Integer> stream;

    public StreamMethod(Stream<Integer> stream){
        this.stream = stream;
        this.integerList = integerList;
        statistics = new ArrayList<>();
    }

    public List<Integer> countStats(){
        Integer maxValue = stream.max(Integer::compare).get();
        Integer minValue = stream.min(Integer::compare).get();
        Double average = stream
                .mapToInt(integerList -> integerList)
                .average()
                .orElse(0.0);

        Integer sumReduce = stream.reduce(0, Integer::sum);
        Integer sumMethod = stream.mapToInt(Integer::intValue).sum();
        System.out.println("Max: " + maxValue);
        System.out.println("Min: " + minValue);
        System.out.println("Average: " + average);
        System.out.println("Sum by reduce : " + sumReduce);
        System.out.println("Sum by method: " + sumMethod);

        return statistics;
    }

    public void sumBiggerThanAverage(){
        Double average = stream
                .mapToInt(integerList -> integerList)
                .average()
                .orElse(0.0);

        Integer sum = stream
                .filter(i -> i > average)
                .mapToInt(i -> i )
                .sum();

        System.out.println("Sum of elements bigger than averange: " + sum);
    }

}
